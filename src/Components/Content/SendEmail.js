/* eslint-disable no-unused-vars */
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  content: {
    padding: theme.spacing(10, 5),
    textAlign: 'center'
  }
}));

const SendEmail = () => {
  const classes = useStyles();

  return (
    <div className={classes.content}>
      <Typography variant="h1">
        Send Email
      </Typography>
      <Typography paragraph>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Similique, vero.
      </Typography>
    </div>
  );
}

export default SendEmail;