import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  content: {
    padding: theme.spacing(10, 5),
    textAlign: 'center'
  }
}));

const Important = () => {
  const classes = useStyles();

  return (
    <div className={classes.content}>
      <Typography variant="h1">
        Important
      </Typography>
      <Typography paragraph>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Similique, vero.
      </Typography>
    </div>
  );
}

export default Important;