import InboxIcon from '@material-ui/icons/MoveToInbox';
import StarBorder from '@material-ui/icons/StarBorder';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import FolderIcon from '@material-ui/icons/Folder';

export const SideBarConfig =
{
  "menu": [
    {
      "name": "Inbox",
      "path": "/inbox",
      "icon": <InboxIcon />,
      "description": "lorem20",
      "key": "0"
    },
    {
      "name": "Starred",
      "icon": <StarBorder />,
      "description": "lorem30",
      "key": "1",
      "children": [{
        "name": "Important",
        "path": "/important",
        "icon": <FolderIcon />,
        "key": "4"

      },
      {
        "name": "Disabled",
        "path": "/disabled",
        "icon": <FolderIcon />,
        "isDisabled": true,
        "key": "5"
      },
      {
        "name": "Attention",
        "path": "/attention",
        "icon": <FolderIcon />,
        "key": "6"
      }]
    },
    {
      "name": "Send Email",
      "path": "/send",
      "icon": <SendIcon />,
      "description": "lorem40",
      "key": "2"
    },
    {
      "name": "Drafts",
      "path": "/drafts",
      "icon": <DraftsIcon />,
      "description": "lorem30",
      "key": "3"
    }
  ]
}