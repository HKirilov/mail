/* eslint-disable no-unused-vars */
import { useEffect, useState } from "react";
import SideBarList from "./SideBarList";
import { SideBarConfig } from './SideBarConfig';

const GenerateList = () => {
  const [lists, setLists] = useState(SideBarConfig);

  return (
    <div>
      <SideBarList lists={lists} />
    </div>
  );
}

export default GenerateList;