/* eslint-disable array-callback-return */
/* eslint-disable no-unused-vars */
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import Collapse from '@material-ui/core/Collapse';
import { useState } from 'react';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
  nested: {
    paddingLeft: theme.spacing(4)
  }
}));

const CollapseMenu = ({ children }) => {
  const [open, setOpen] = useState(true);
  const classes = useStyles();

  return (
    <div>
      {children && children.map(child => (
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding key={child.key}>
            <Link to={child.path}>
              <ListItem button disabled={child.isDisabled} className={classes.nested}>
                <ListItemIcon>
                  {child.icon}
                </ListItemIcon>
                <ListItemText primary={child.name} />
              </ListItem>
            </Link>
          </List>
        </Collapse>
      ))}
    </div >
  );
}

export default CollapseMenu;