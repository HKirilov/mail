import { List, ListItem, ListItemIcon, ListItemText } from "@material-ui/core";
import CollapseMenu from "./CollapseMenu";
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import { useState } from "react";
import { Link } from "react-router-dom";

const SideBarList = ({ lists }) => {
  const [open, setOpen] = useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  function checkForChildren(list) {
    const children = list.children;
    if (children) {
      return open ?
        <ExpandLess onClick={handleClick} />
        :
        <ExpandMore onClick={handleClick} />
    }
  }

  function collapse(list) {
    // console.log(list);
    const children = list.children;
    if (open === true) {
      return <CollapseMenu children={children} />
    }
  }

  return (
    <div>
      {lists.menu.map(list => (
        <List key={list.key}>
          <Link to={list.path}>
            <ListItem button>
              <ListItemIcon>
                {list.icon}
              </ListItemIcon>
              <ListItemText primary={list.name} />
              {checkForChildren(list)}
            </ListItem>
          </Link>
          {collapse(list)}
        </List>
      ))}
    </div>
  );
}

export default SideBarList;