import Footer from "./Components/Footer";
import Header from "./Components/Header";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Inbox from "./Components/Content/Inbox";
import Important from './Components/Content/Important';
import Attention from "./Components/Content/Attention";
import SendEmail from "./Components/Content/SendEmail";
import Drafts from "./Components/Content/Drafts";
import Home from "./Components/Content/Home";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <div className="content">
          <Switch>
            <Route exact path="/home">
              <Home />
            </Route>
            <Route path="/inbox">
              <Inbox />
            </Route>
            <Route path="/important">
              <Important />
            </Route>
            <Route path="/attention">
              <Attention />
            </Route>
            <Route path="/send">
              <SendEmail />
            </Route>
            <Route path="/drafts">
              <Drafts />
            </Route>
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
